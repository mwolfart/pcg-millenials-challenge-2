﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    private GameObject audioController;

    public void Awake() {
        audioController = GameObject.FindGameObjectWithTag("Audio");
    }

    public void PlaySound(string path) {
        if (!GameConfig.soundOff && audioController != null) {
            AudioClip sound = Resources.Load<AudioClip>(path);
            AudioSource[] audioSources = audioController.GetComponents<AudioSource>();
            AudioSource soundSrc;

            if (GameConfig.isPlayingMusic && audioSources.Length > 1) {
                soundSrc = audioSources[1];
            } else if (!GameConfig.isPlayingMusic) {
                soundSrc = audioSources[0];
            } else return;

            soundSrc.clip = sound;
            soundSrc.Play();
        }
    }

    public void PlayMusic(string path) {
        if (!GameConfig.musicOff && audioController != null
            && (!GameConfig.isPlayingMusic || (GameConfig.isPlayingMusic && GameConfig.curPlayingMusic != path))) {
            GameConfig.isPlayingMusic = true;
            GameConfig.curPlayingMusic = path;
            AudioClip music = Resources.Load<AudioClip>(path);
            AudioSource musicSrc = audioController.GetComponent<AudioSource>();
            musicSrc.clip = music;
            musicSrc.loop = true;
            musicSrc.Play();
        }
    }

    public void StopMusic() {
        if (!GameConfig.musicOff && GameConfig.isPlayingMusic && audioController != null) {
            AudioSource musicSrc = audioController.GetComponent<AudioSource>();
            musicSrc.loop = false;
            musicSrc.Stop();
            GameConfig.isPlayingMusic = false;
            GameConfig.curPlayingMusic = "";
        }
    }
}
