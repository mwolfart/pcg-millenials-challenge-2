﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraZoom : MonoBehaviour
{
    private ThirdPersonCamera camera;

    void Start() {
        camera = gameObject.GetComponent<ThirdPersonCamera>();
    }

    void FixedUpdate() {   
        float mouseScroll = Input.GetAxis("Mouse ScrollWheel");
        if (mouseScroll > 0f && camera.distance > 2) {
            camera.distance -= .25f;
        } else if (mouseScroll < 0f && camera.distance < 8) {
            camera.distance += .25f;
        }
    }
}
