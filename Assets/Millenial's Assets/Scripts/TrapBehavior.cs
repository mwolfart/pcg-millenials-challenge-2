﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapBehavior : MonoBehaviour
{
    public bool Active = true;
    public GameObject TrapBlock;
    private BoxCollider blockCollider;

    void Start() {
        blockCollider = TrapBlock.GetComponentInChildren<BoxCollider>();
    } 

    void Update() {
        if (Active && TrapBlock && TrapBlock.transform.eulerAngles.x < 75) {
            TrapBlock.transform.Rotate(new Vector3(5, 0, 0), Space.Self);
        } else if (Active && TrapBlock && TrapBlock.transform.eulerAngles.x >= 75 && blockCollider.enabled) {
            blockCollider.enabled = false;
        } else if (!Active && TrapBlock && TrapBlock.transform.eulerAngles.x > 0) {
            TrapBlock.transform.Rotate(new Vector3(-5, 0, 0), Space.Self);
        } else if (!Active && TrapBlock && TrapBlock.transform.eulerAngles.x <= 0 && !blockCollider.enabled) {
            blockCollider.enabled = true;
        }
    }
}
