﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCamera : MonoBehaviour
{
    public GameObject Target;
     
    void Update() {
        transform.LookAt(Target.transform);
        transform.Translate(Vector3.right * Time.deltaTime * 4);
    }

}
