using System.Collections;
using UnityEngine;

public static class GameConfig {
    public static bool musicOff { get; set; }
    public static bool soundOff { get; set; }
    public static bool isPlayingMusic { get; set; }
    public static string curPlayingMusic { get; set; }
}