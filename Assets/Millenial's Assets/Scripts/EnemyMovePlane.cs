﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovePlane : MonoBehaviour {
    public float speed;
    public bool isHorizontal = false;
    public bool freezeMovement;
    private bool reverse;
    private float size;
    private float previousContactY = .005f;

    void Start() {
        if (GetComponent<EnemyRotate>() != null) {
            GetComponent<EnemyRotate>().isHorizontal = isHorizontal;
        }
        size = transform.localScale.x;
    }

    void Update() {
        if (freezeMovement)
            return;

        // TODO Adapt to other processors
        float translatedValue = 2 * Time.deltaTime * speed;
        if (reverse) {
            if (isHorizontal) translateX(translatedValue); else translateZ(translatedValue);
            transform.localScale = new Vector3(size, size, size);
        } else {
            if (isHorizontal) {
                translateX(-translatedValue);
                reverseScaleX();
            } else {
                translateZ(-translatedValue);
                reverseScaleZ();
            }
        }
    }

    void OnCollisionEnter(Collision col) {
        if (!tryStepping(col.contacts[0].point.y) 
            && col.gameObject.tag != "Player" 
            && col.gameObject.tag != "Ground") 
        {
            // Can not simply get !reverse because if object collides with two blocks
            //  at same time, it will reverse 2 times (not reversing at all).
            reverse = getOppositeDirection(col.contacts);
            if (GetComponent<EnemyRotate>() != null) {
                GetComponent<EnemyRotate>().reverse = reverse;
            }
        }
    }

    private bool tryStepping(float collisionContactY) {
        float stepAmount = collisionContactY - previousContactY;
        previousContactY = collisionContactY;
        if (stepAmount > 0 && stepAmount < .02f) {
            transform.position = Vec3.SetY(transform.position, transform.position.y + stepAmount);
            return true;
        }
        return false;
    }

    private bool getOppositeDirection(ContactPoint[] contactPoints) {
        if (isHorizontal) {
            return contactPoints[0].point.x < transform.position.x;
        } else {
            return contactPoints[0].point.z < transform.position.z;
        }
    }

    private void translateZ(float value) {
        transform.Translate(0, 0, value, Space.World);
    }

    private void translateX(float value) {
        transform.Translate(value, 0, 0, Space.World);
    }

    private void reverseScaleZ() {
        transform.localScale = new Vector3(size, size, -size);
    }

    private void reverseScaleX() {
        transform.localScale = new Vector3(-size, size, size);
    }
}
