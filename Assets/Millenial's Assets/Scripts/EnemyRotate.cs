﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyRotate : MonoBehaviour {
    public float speed;
    public bool isHorizontal = false;
    public bool reverse;
    public bool freezeMovement;

    void Update() {
        if (!freezeMovement) {
            float rotatedValue = 10 * Time.deltaTime * speed;
            if (reverse) {
                if (isHorizontal) rotateZ(-rotatedValue); else rotateX(rotatedValue);
            } else {
                if (isHorizontal) rotateZ(rotatedValue); else rotateX(-rotatedValue);
            }
        }
    }

    private void rotateX(float value) {
        transform.Rotate(value, 0, 0);
    }

    private void rotateZ(float value) {
        transform.Rotate(0, 0, value);
    }
}
