﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillbugBody : MonoBehaviour
{
    public int colliding = 0;
    private float previousContactY = .005f;
    private string[] ignoredCollisions = { "Player", "Ground", "Trap", "Water" };

    private void OnCollisionEnter(Collision col) {
        if (!tryStepping(col.contacts[0].point.y) && !arrayContains(ignoredCollisions, col.gameObject.tag)) {
            colliding++;
        }
    }

    private void OnCollisionExit(Collision col) {
        if (!arrayContains(ignoredCollisions, col.gameObject.tag)) {
            colliding--;
        }
    }

    private bool tryStepping(float collisionContactY) {
        float stepAmount = collisionContactY - previousContactY;
        previousContactY = collisionContactY;
        if (stepAmount > 0 && stepAmount < .02f) {
            transform.position = Vec3.SetY(transform.position, transform.position.y + stepAmount);
            return true;
        }
        return false;
    }

    private bool arrayContains(string[] strArr, string elem) {
        foreach (string s in strArr) {
            if (s == elem)
                return true;
        }
        return false;
    }
}
