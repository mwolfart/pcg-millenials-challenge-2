﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PillbugPivot : MonoBehaviour
{
    public bool colliding = true;

    private string[] ignoredCollisions = { "Player", "Ground", "Trap", "Water" };

    private void OnTriggerStay(Collider col) {
        if (!arrayContains(ignoredCollisions, col.gameObject.tag)) {
            //bool distanceTooClose = Utils.testDistanceBetweenCenters(transform.position, col.transform.position, 1.0f);
            //colliding = distanceTooClose;
            colliding = true;
        }
    }

    private void OnTriggerExit(Collider col) {
        if (!arrayContains(ignoredCollisions, col.gameObject.tag)) {
            colliding = false;
        }
    }

    private bool arrayContains(string[] strArr, string elem) {
        foreach(string s in strArr) {
            if (s == elem)
                return true;
        }
        return false;
    }
}
