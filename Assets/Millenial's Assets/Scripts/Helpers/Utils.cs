using UnityEngine;

public static class Utils {
    public static bool testDistanceBetweenCenters(Vector3 blockPosition, Vector3 colliderPosition, float targetDistance) {
        Vector2 squareBlockCenter = new Vector2(blockPosition.x, blockPosition.z);
        Vector2 squareColliderCenter = new Vector2(colliderPosition.x, colliderPosition.z);
        return Vector2.Distance(squareBlockCenter, squareColliderCenter) <= targetDistance;
        // return Mathf.Round(Vector2.Distance(squareBlockCenter, squareColliderCenter) * 100f) / 100f <= targetDistance;
    }
}