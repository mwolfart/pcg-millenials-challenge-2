using UnityEngine;

public class Vec3 {
    public static Vector3 SetX(Vector3 vec, float val) {
        Vector3 newVec = vec;
        vec.x = val;
        return vec;
    }

    public static Vector3 SetY(Vector3 vec, float val) {
        Vector3 newVec = vec;
        vec.y = val;
        return vec;
    }

    public static Vector3 SetZ(Vector3 vec, float val) {
        Vector3 newVec = vec;
        vec.z = val;
        return vec;
    }

    public static Vector3 ClearX(Vector3 vec) {
        return SetX(vec, 0);
    }

    public static Vector3 ClearY(Vector3 vec) {
        return SetY(vec, 0);
    }

    public static Vector3 ClearZ(Vector3 vec) {
        return SetZ(vec, 0);
    }

    public static Vector3 ClearXY(Vector3 vec) {
        Vector3 v = SetX(vec, 0);
        return SetY(v, 0);
    }

    public static Vector3 ClearXZ(Vector3 vec) {
        Vector3 v = SetX(vec, 0);
        return SetZ(v, 0);
    }

    public static Vector3 ClearYZ(Vector3 vec) {
        Vector3 v = SetY(vec, 0);
        return SetZ(v, 0);
    }
}