﻿using System;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public enum KeyColor {
    Red,
    Green,
    Blue,
    Yellow,
    Silver
}

public enum ShoeType {
    Fire,
    Suction,
    Water,
    Ice,
    Float
}

public class LevelBehavior : MonoBehaviour
{
    // Level properties
    public int LocationNumber;
    public int AreaNumber;
    public string AreaTitle;
    public int TotalWifis;
    public int Timelimit;

    // Texts
    private GameObject txtLevelLabel;
    private GameObject txtLevelTitle;
    private GameObject txtTimelimit;
    private GameObject txtCollectedWifi;
    private GameObject txtTotalWifi;

    // Inventory icons
    private GameObject[] keyIcons;
    private GameObject[] keyIconCounters;
    private GameObject[] shoeIcons;

    // Inventory flags
    private int collectedWifi = 0;
    private int[] collectedKeys = { 0, 0, 0, 0, 0 };
    private bool[] collectedShoes = { false, false, false, false, false };

    // Level flags
    public bool toggleFlag = false;

    // Other
    public int curTime;
    private float prevTime;
    private bool stopTimer = false;

    // Start is called before the first frame update
    void Start() {
        keyIcons = new GameObject[5];
        keyIconCounters = new GameObject[5];
        shoeIcons = new GameObject[5];

        GameObject[] hudObjects = GameObject.FindGameObjectsWithTag("HUDObject");
        assignHudObjects(hudObjects);
        ClearInventory();
        
        txtLevelLabel.GetComponent<UnityEngine.UI.Text>().text = "Location " + LocationNumber.ToString("00") + " - Area " + AreaNumber.ToString("00");
        txtLevelTitle.GetComponent<UnityEngine.UI.Text>().text = AreaTitle;
        txtTimelimit.GetComponent<UnityEngine.UI.Text>().text = Timelimit.ToString("000");
        txtCollectedWifi.GetComponent<UnityEngine.UI.Text>().text = "00";
        txtTotalWifi.GetComponent<UnityEngine.UI.Text>().text = TotalWifis.ToString("00");

        curTime = Timelimit;
        prevTime = 0.0f;

        gameObject.GetComponent<SoundPlayer>().PlayMusic("Millenials/Audio/oxygene14");
    }

    // Update is called once per frame
    void Update() {
        // Timer
        if (stopTimer)
            return;
        
        prevTime += Time.deltaTime;
        if (prevTime >= 1 && curTime > 0) {
            prevTime = 0.0f;
            curTime--;
            txtTimelimit.GetComponent<UnityEngine.UI.Text>().text = curTime.ToString("000");
        }
    }

    public void AddInventoryWifi() {
        collectedWifi++;
        txtCollectedWifi.GetComponent<UnityEngine.UI.Text>().text = collectedWifi.ToString("00");

        if (collectedWifi >= TotalWifis) {
            txtCollectedWifi.GetComponent<UnityEngine.UI.Text>().color = new Color32(61, 168, 255, 255);
        }
    }

    public void AddInventoryKey(KeyColor color) {
        int colorId = (int) color;
        collectedKeys[colorId]++;

        if (collectedKeys[colorId] == 1) {
            keyIcons[colorId].SetActive(true);
        } else {
            keyIconCounters[colorId].SetActive(true);
            keyIconCounters[colorId].GetComponent<UnityEngine.UI.Text>().text = collectedKeys[colorId].ToString("00");
        }
    }

    public void AddInventoryShoe(ShoeType shoe) {
        int shoeId = (int)shoe;
        collectedShoes[shoeId] = true;
        shoeIcons[shoeId].SetActive(true);
    }

    public bool HasInventoryKey(KeyColor color) {
        int colorId = (int)color;
        return (collectedKeys[colorId] > 0);
    }

    public bool HasInventoryShoe(ShoeType shoe) {
        int shoeId = (int)shoe;
        return collectedShoes[shoeId];
    }

    public void TakeInventoryKey(KeyColor color) {
        int colorId = (int)color;
        collectedKeys[colorId]--;

        if (collectedKeys[colorId] < 2)
            keyIconCounters[colorId].SetActive(false);
        else keyIconCounters[colorId].GetComponent<UnityEngine.UI.Text>().text = collectedKeys[colorId].ToString("00");
        
        if (collectedKeys[colorId] < 1) 
            keyIcons[colorId].SetActive(false);
    }

    public void ClearInventory() {
        foreach (GameObject iconObj in keyIcons.Concat(keyIconCounters).Concat(shoeIcons).ToArray()) {
            iconObj.SetActive(false);
        }
        collectedKeys = new int[] { 0, 0, 0, 0, 0 };
        collectedShoes = new bool[] { false, false, false, false, false };
    }

    public bool HasAllWifis() {
        return (collectedWifi >= TotalWifis);
    }

    public void StopTimer() {
        stopTimer = true;
    }

    // TODO Maybe we can refactor this?
    void assignHudObjects(GameObject[] hudObjList) {
        foreach (GameObject hudObj in hudObjList) {
            switch (hudObj.name) {
                case "TxtLevelId":
                    txtLevelLabel = hudObj;
                    break;
                case "TxtLevelTitle":
                    txtLevelTitle = hudObj;
                    break;
                case "TxtTimeCounter":
                    txtTimelimit = hudObj;
                    break;
                case "TxtCollectedWifi":
                    txtCollectedWifi = hudObj;
                    break;
                case "TxtTotalWifi":
                    txtTotalWifi = hudObj;
                    break;
                case "IconKeyRed":
                    keyIcons[0] = hudObj;
                    break;
                case "IconKeyGreen":
                    keyIcons[1] = hudObj;
                    break;
                case "IconKeyBlue":
                    keyIcons[2] = hudObj;
                    break;
                case "IconKeyYellow":
                    keyIcons[3] = hudObj;
                    break;
                case "IconKeySilver":
                    keyIcons[4] = hudObj;
                    break;
                case "TxtKeyRedCount":
                    keyIconCounters[0] = hudObj;
                    break;
                case "TxtKeyGrnCount":
                    keyIconCounters[1] = hudObj;
                    break;
                case "TxtKeyBluCount":
                    keyIconCounters[2] = hudObj;
                    break;
                case "TxtKeyYlwCount":
                    keyIconCounters[3] = hudObj;
                    break;
                case "TxtKeySlvCount":
                    keyIconCounters[4] = hudObj;
                    break;
                case "IconShoeFire":
                    shoeIcons[0] = hudObj;
                    break;
                case "IconShoeSuction":
                    shoeIcons[1] = hudObj;
                    break;
                case "IconShoeWater":
                    shoeIcons[2] = hudObj;
                    break;
                case "IconShoeIce":
                    shoeIcons[3] = hudObj;
                    break;
                case "IconShoeFloat":
                    shoeIcons[4] = hudObj;
                    break;
            }
        }
    }

    /* This is the simplified version, however, we cannot count on the order of FindObjectsWithTag
    void assignHudObjects(GameObject[] hudObjects) {
        txtLevelLabel = hudObjects[0];
        txtLevelTitle = hudObjects[1];
        txtTimelimit = hudObjects[2];
        txtCollectedWifi = hudObjects[3];
        txtTotalWifi = hudObjects[4];

        keyIcons = hudObjects.Skip(5).Take(5).ToArray();
        keyIconCounters = hudObjects.Skip(10).Take(5).ToArray();
        shoeIcons = hudObjects.Skip(15).Take(5).ToArray();
    }
    */
}
