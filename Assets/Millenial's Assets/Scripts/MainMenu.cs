﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject WifiIcon;
    public GameObject MainMenuObj;
    public GameObject OptionsMenuObj;
    public GameObject ChooseLvlMenuObj;
    public GameObject txtSoundStatus;
    public GameObject txtMusicStatus;
    private int menuChoice = 0;
    private float lastTime = 0.0f;

    void Start() {
        GameConfig.isPlayingMusic = false;
        txtSoundStatus.GetComponent<UnityEngine.UI.Text>().text = !GameConfig.soundOff ? "ON" : "OFF";
        txtMusicStatus.GetComponent<UnityEngine.UI.Text>().text = !GameConfig.musicOff ? "ON" : "OFF";
    }

    void Update() {
        //10, -30, -70
        WifiIcon.transform.localPosition = Vec3.SetY(WifiIcon.transform.localPosition, 10 - menuChoice * 40);
    }

    void FixedUpdate() {
        // TODO adapt to other processors
        if (Time.time - lastTime < .2f)
            return;

        if (Input.GetKey("up") && menuChoice > 0) {
            lastTime = Time.time;
            menuChoice--;
            gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/menucursor");
        } else if (Input.GetKey("down") && menuChoice < getMaxMenuItems()) {
            lastTime = Time.time;
            menuChoice++;
            gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/menucursor");
        } else if (Input.GetKey("return") || Input.GetKey("enter")) {
            lastTime = Time.time;
            selectChoice();
            gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/menuenter");
        }
    }

    private void selectChoice() {
        if (MainMenuObj && MainMenuObj.activeSelf) {
            switch (menuChoice) {
                case 0:
                    SceneManager.LoadScene("level1");
                    break;
                case 1:
                    menuChoice = 0;
                    MainMenuObj.SetActive(false);
                    ChooseLvlMenuObj.SetActive(true);
                    break;
                case 2:
                    menuChoice = 0;
                    MainMenuObj.SetActive(false);
                    OptionsMenuObj.SetActive(true);
                    break;
                case 3:
                    Application.Quit();
                    break;
            }
        } else if (OptionsMenuObj && OptionsMenuObj.activeSelf) {
            switch (menuChoice) {
                case 0:
                    GameConfig.soundOff = !GameConfig.soundOff;
                    txtSoundStatus.GetComponent<UnityEngine.UI.Text>().text = !GameConfig.soundOff ? "ON" : "OFF";
                    break;
                case 1:
                    GameConfig.musicOff = !GameConfig.musicOff;
                    txtMusicStatus.GetComponent<UnityEngine.UI.Text>().text = !GameConfig.musicOff ? "ON" : "OFF";
                    break;
                case 2:
                    menuChoice = 0;
                    MainMenuObj.SetActive(true);
                    OptionsMenuObj.SetActive(false);
                    break;
                default:
                    break;
            }
        } else if (ChooseLvlMenuObj && ChooseLvlMenuObj.activeSelf) {
            switch (menuChoice) {
                case 0:
                case 1:
                case 2:
                    SceneManager.LoadScene("level" + (menuChoice + 1));
                    break;
                case 3:
                    menuChoice = 0;
                    MainMenuObj.SetActive(true);
                    ChooseLvlMenuObj.SetActive(false);
                    break;
                default:
                    break;
            }
        }
    }

    private int getMaxMenuItems() {
        if (MainMenuObj && MainMenuObj.activeSelf) {
            return 3;
        } else if (OptionsMenuObj && OptionsMenuObj.activeSelf) {
            return 2;
        } else if (ChooseLvlMenuObj && ChooseLvlMenuObj.activeSelf) {
            return 3;
        } else return 0;
    }
}
