﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerInteractions : MonoBehaviour
{
    public GameObject FloorUnderDirtPrefab;

    private GameObject HUD;
    private LevelBehavior levelBehavior;
    private LevelTransitions levelTransitions;

    private GameObject bloodParticles;
    private GameObject dirtParticles;
    private GameObject waterParticles;
    private GameObject fireParticles;
    private GameObject bombParticles;

    private bool enableRestart = false;

    void Start() {
        // HUD and Level related
        HUD = GameObject.FindGameObjectWithTag("HUD");
        levelBehavior = HUD.GetComponent<LevelBehavior>();
        levelTransitions = HUD.GetComponent<LevelTransitions>();

        // Particles
        bloodParticles = Resources.Load<GameObject>("Millenials/Particles/Blood Particles");
        dirtParticles  =  Resources.Load<GameObject>("Millenials/Particles/Dirt Particles");
        waterParticles = Resources.Load<GameObject>("Millenials/Particles/Water Particles");
        fireParticles  =  Resources.Load<GameObject>("Millenials/Particles/Fire Particles");
        bombParticles = Resources.Load<GameObject>("Millenials/Particles/Bomb Particles");
    }

    void Update() {
        if (enableRestart && Input.GetKeyDown("space")) {
            enableRestart = false;
            // TODO similar code present in LevelTransitions. refactor
            int location = (HUD.GetComponent<LevelBehavior>().LocationNumber - 1) * 4;
            int area = HUD.GetComponent<LevelBehavior>().AreaNumber;
            int curLevel = location + area;
            SceneManager.LoadScene("level" + curLevel);
        }

        if (HUD.GetComponent<LevelBehavior>().curTime <= 0 && !enableRestart)
        {
            killCharacter();
            gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/alert");
        }
    }

    void OnCollisionEnter(Collision collision) {
        switch(collision.gameObject.tag) {
            case "Wifi":
                Destroy(collision.gameObject);
                levelBehavior.AddInventoryWifi();
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/blip2");
                break;
            case "Key":
                Destroy(collision.gameObject);
                string strKeyColor = collision.gameObject.name.Split(null)[0];
                KeyColor keyColor = (KeyColor)Enum.Parse(typeof(KeyColor), strKeyColor);
                levelBehavior.AddInventoryKey(keyColor);
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/blip2");
                break;
            case "Shoe":
                Destroy(collision.gameObject);
                string strShoeType = collision.gameObject.name.Split(null)[0];
                ShoeType shoe = (ShoeType)Enum.Parse(typeof(ShoeType), strShoeType);
                levelBehavior.AddInventoryShoe(shoe);
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/blip2");
                break;
            case "Door":
                string strDoorColor = collision.gameObject.name.Split(null)[1];
                KeyColor doorColor = (KeyColor)Enum.Parse(typeof(KeyColor), strDoorColor);
                if (levelBehavior.HasInventoryKey(doorColor)) {
                    if (doorColor != KeyColor.Silver) {
                        levelBehavior.TakeInventoryKey(doorColor);
                    }
                    Destroy(collision.gameObject);
                    gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/door");
                }
                break;
            case "Socket":
                if (levelBehavior.HasAllWifis()) { 
                    Destroy(collision.gameObject);
                    gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/exploson");
                }
                break;
            case "Bomb":
                killCharacter("Bomb");
                Destroy(collision.gameObject);
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/jezzdead");
                break;
            case "Ground":
                gameObject.transform.position = Vec3.SetY(gameObject.transform.position, collision.transform.position.y);
                break;
            case "Lift":
                collision.gameObject.GetComponent<LiftBehavior>().ToggleRaised();
                break;
            case "Enemy":
                killCharacter();
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/jezzdead");
                break;
            default:
                levelTransitions.OnSpecialCollisionEnter(collision);
                break;
        }
    }

    void OnCollisionExit(Collision collision) {
        switch (collision.gameObject.tag) {
            default:
                break;
        }
    }

    void OnCollisionStay(Collision collision) {
        switch (collision.gameObject.tag) {
            case "Trap":
                if (collision.gameObject.GetComponent<TrapBehavior>().Active) {
                    gameObject.transform.position = Vec3.SetX(gameObject.transform.position, collision.gameObject.transform.position.x);
                    gameObject.transform.position = Vec3.SetZ(gameObject.transform.position, collision.gameObject.transform.position.z);
                } else {
                    gameObject.transform.position = Vec3.ClearY(gameObject.transform.position);
                }
                break;
            default:
                break;
        }
    }

    void OnTriggerEnter(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Dirt":
                testDirtCollision(trigger);
                break;
            case "Fire":
                testFireCollision();
                break;
            case "Button":
                trigger.gameObject.GetComponent<ButtonBehavior>().PressButton();
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/pop2");
                break;
            default:
                break;
        }
    }

    void OnTriggerStay(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Water":
                testWaterCollision(trigger);
                break;
            default:
                break;
        }
    }

    void OnTriggerExit(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Button":
                trigger.gameObject.GetComponent<ButtonBehavior>().ReleaseButton();
                break;
            default:
                break;
        }
    }

    private void killCharacter(string mode = "") {
        Vector3 particlePosition = Vec3.SetY(gameObject.transform.position, 0.5f);
        spawnParticles(particlePosition, mode);

        hideCharacterAndDeactivateControls();

        // Stop HUD
        levelBehavior.StopTimer();
        enableRestart = true;
    }

    private void hideCharacterAndDeactivateControls() {
        gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = false;
        gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonCharacter>().enabled = false;
        gameObject.GetComponent<CapsuleCollider>().enabled = false;
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        gameObject.GetComponent<Rigidbody>().velocity = new Vector3();

        for (int chId = 0; chId < transform.childCount; chId++) {
            Transform child = transform.GetChild(chId);
            child.gameObject.SetActive(false);
        }
    }

    private void spawnParticles(Vector3 particlePosition, string particleType = "") {
        GameObject particlePrefab = getDeathParticlePrefab(particleType);
        GameObject particles = Instantiate(particlePrefab, particlePosition, Quaternion.identity);
        particles.GetComponent<ParticleSystem>().Play();
    }

    private void testWaterCollision(Collider waterTrigger) {
        bool distanceTooClose = transform.position.y + 0.5f < waterTrigger.transform.position.y;
        if (!distanceTooClose)
            return;

        if (!levelBehavior.HasInventoryShoe(ShoeType.Water)) {
            gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/water2");
            killCharacter("Water");
        } else if (transform.position.y > -0.56f) {
            gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/water2");
            Vector3 particlePosition = Vec3.SetY(gameObject.transform.position, 0.25f);
            spawnParticles(particlePosition, "Water");
        }
    }

    private void testFireCollision() {
        if (!levelBehavior.HasInventoryShoe(ShoeType.Fire))
            killCharacter("Fire");
    }

    private GameObject getDeathParticlePrefab(string mode = "") {
        switch (mode) {
            case "Water":
                return waterParticles;
            case "Fire":
                return fireParticles;
            case "Bomb":
                return bombParticles;
            default:
                return bloodParticles;
        }
    }

    private void testDirtCollision(Collider dirtTrigger) {
        GameObject dirtObject = dirtTrigger.gameObject.transform.parent.gameObject;
        Vector3 triggerPosition = dirtTrigger.gameObject.transform.position;
        removeDirt(dirtObject, triggerPosition);
    }

    private void removeDirt(GameObject dirtObject, Vector3 dirtPosition) {
        // Add particles
        GameObject particles = Instantiate(dirtParticles, dirtPosition, Quaternion.identity);
        particles.GetComponent<ParticleSystem>().Play();
        // Add floor
        Instantiate(FloorUnderDirtPrefab, Vec3.ClearY(dirtPosition), Quaternion.identity);
        // Destroy dirt
        Destroy(dirtObject);
    }
}
