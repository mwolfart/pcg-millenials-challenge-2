﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelTransitions : MonoBehaviour {
    private GameObject levelIntro;
    private GameObject levelFakeI;
    private GameObject levelPart1;
    private GameObject levelFake1;
    private GameObject levelPart2;
    private GameObject levelFake2;
    private GameObject levelPart3;

    private GameObject HUD;
    private GameObject HUDFader;
    private LevelBehavior levelBehavior;

    private int partToReveal = 0;
    private bool enableScreenTransition = false;
    private float screenCoverAlpha = 0.0f;

    public bool hideFake1 = true;
    public bool hideFake2 = true;

    public void Start() {
        levelIntro = GameObject.FindGameObjectWithTag("LevelIntro");
        levelFakeI = GameObject.FindGameObjectWithTag("LevelFakeI");
        levelPart1 = GameObject.FindGameObjectWithTag("LevelPart1");
        levelFake1 = GameObject.FindGameObjectWithTag("LevelFake1");
        levelPart2 = GameObject.FindGameObjectWithTag("LevelPart2");
        levelFake2 = GameObject.FindGameObjectWithTag("LevelFake2");
        levelPart3 = GameObject.FindGameObjectWithTag("LevelPart3");

        if (levelPart1) levelPart1.SetActive(false);
        if (levelPart2) levelPart2.SetActive(false);
        if (levelPart3) levelPart3.SetActive(false);
        if (levelFake1 && hideFake1) levelFake1.SetActive(false);
        if (levelFake2 && hideFake2) levelFake2.SetActive(false);

        HUD = GameObject.FindGameObjectWithTag("HUD");
        levelBehavior = HUD.GetComponent<LevelBehavior>();

        HUDFader = GameObject.FindGameObjectWithTag("HUDFader");
        HUDFader.GetComponent<CanvasRenderer>().SetAlpha(0.0f);
    }

    public void FixedUpdate() {
        performScreenTransition();

        if (Input.GetKey("escape")) {
            gameObject.GetComponent<SoundPlayer>().StopMusic();
            startLevel(0);
        }
    }

    public void OnSpecialCollisionEnter(Collision collision) {
        switch (collision.gameObject.tag) {
            case "WifiTag1":
                Destroy(collision.gameObject);
                levelBehavior.AddInventoryWifi();
                enableScreenTransition = true;
                partToReveal = 1;
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/blip2");
                break;
            case "WifiTag2":
                Destroy(collision.gameObject);
                levelBehavior.AddInventoryWifi();
                enableScreenTransition = true;
                partToReveal = 2;
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/blip2");
                break;
            case "WifiTag3":
                Destroy(collision.gameObject);
                levelBehavior.AddInventoryWifi();
                enableScreenTransition = true;
                partToReveal = 3;
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/blip2");
                break;
            case "Smart1":
                Destroy(collision.gameObject);
                enableScreenTransition = true;
                partToReveal = 4;
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/blip2");
                break;
            default:
                break;
        }
    }

    public void startLevel(int lvlNo){
      SceneManager.LoadScene("level" + lvlNo);
    }

    private void revealLevelPart() {
        switch(partToReveal) {
            case 1:
                revealPart1();
                break;
            case 2:
                revealPart2();
                break;
            case 3:
                revealPart3();
                break;
            case 4:
                // Next level
                // TODO similar code present in PlayerInteractions. refactor
                int location = (HUD.GetComponent<LevelBehavior>().LocationNumber - 1) * 4;
                int area = HUD.GetComponent<LevelBehavior>().AreaNumber;
                int curLevel = location + area;
                if (curLevel > 2) {
                    curLevel = -1;
                    gameObject.GetComponent<SoundPlayer>().StopMusic();
                }
                startLevel(curLevel + 1);
                break;
        }
    }

    private void revealPart1() {
        levelFakeI.SetActive(false);
        levelPart1.SetActive(true);
        if (hideFake1 && levelFake1) levelFake1.SetActive(true);
    }

    private void revealPart2() {
        levelFake1.SetActive(false);
        levelPart2.SetActive(true);
        if (hideFake2 && levelFake2) levelFake2.SetActive(true);
    }

    private void revealPart3() {
        levelFake2.SetActive(false);
        levelPart3.SetActive(true);
    }

    private void performScreenTransition() {
        if (enableScreenTransition) {
            if (screenCoverAlpha < 1) {
                screenCoverAlpha += 0.1f;
                HUDFader.GetComponent<CanvasRenderer>().SetAlpha(screenCoverAlpha);
            } else enableScreenTransition = false;
        } else if (screenCoverAlpha > 0) {
            revealLevelPart();
            screenCoverAlpha -= 0.1f;
            HUDFader.GetComponent<CanvasRenderer>().SetAlpha(screenCoverAlpha);
        }
    }
}
