﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWallMove : MonoBehaviour
{
    public float speed;
    public bool freezeMovement;
    public bool debug = false;

    private float rotationDelay = 0;
    private int colliding = 0;
    private float previousContactY = .005f;
    private GameObject prevCollidedObj;
    private string[] ignoredCollisions = { "Player", "Ground", "Trap", "Water" };
    //private Collider previousCollider;

    private int direction = 0;

    void Start() {
        int rotationY = roundRotation(transform.eulerAngles.y);
        switch (rotationY) {
            case 90:
            case -270:
                direction = 3;
                break;
            case 0:
            case 360:
                direction = 2;
                break;
            case 270:
            case -90:
                direction = 1;
                break;
            default:
                direction = 0;
                break;
        }
    }

    void Update() {
        if (freezeMovement)
            return;
        
        testCorner();

        // TODO Adapt to other processors
        float translatedValue = 2 * Time.deltaTime * speed;
        moveBody(translatedValue);
    }

    private void OnCollisionEnter(Collision col) {
        if ( /* !tryStepping(col.contacts) */ // TODO remove once buggy behavior is fixed
            !arrayContains(ignoredCollisions, col.gameObject.tag) 
            && col.gameObject != prevCollidedObj 
            && collidedInFront(col)) 
        {
            prevCollidedObj = col.gameObject;
            transform.Rotate(0, -90, 0, Space.Self);
            decrementDirection();
        }
    }

    private void testCorner() {
        if (!colliderPresentAtRight() && (Time.time < 1 || Time.time - rotationDelay > .5f)) {
            transform.Rotate(0, 90, 0, Space.Self);

            incrementDirection();
            rotationDelay = Time.time;
        }
    }

    private bool colliderPresentAtRight() {
        Vector3[] origins = getPivotPoints();

        Vector3 forwardVec = getForwardVec();
        Vector3 rightVec = -1 * Vector3.Cross(forwardVec, transform.up.normalized);
        rightVec.y += .1f;

        /*
        if (!(raycastThick(origins[0], rightVec, .75f) || raycastThick(origins[1], rightVec, .75f))) {
            Debug.DrawLine(origins[0], origins[0] + rightVec, new Color(1, 1, 1), 4);
            Debug.DrawLine(origins[1], origins[1] + rightVec, new Color(1, 1, 1), 4);
        }
        */

        return raycastThick(origins[0], rightVec, .75f) || raycastThick(origins[1], rightVec, .75f) || raycastThick(origins[2], rightVec, .75f);
    }

    private bool raycastThick(Vector3 origin, Vector3 direction, float distance) {
        Vector3 originCopy1 = origin;
        Vector3 originCopy2 = origin;
        RaycastHit hit, hit1, hit2;

        return Physics.Raycast(origin, direction, out hit, distance)
            && Physics.Raycast(originCopy1, direction, out hit1, distance)
            && Physics.Raycast(originCopy2, direction, out hit2, distance);
    }

    private bool tryStepping(ContactPoint[] contacts) {
        ContactPoint minimum = contacts[0];
        foreach(ContactPoint pt in contacts) {
            if (pt.point.y < minimum.point.y)
                minimum = pt;
        }
        float collisionContactY = minimum.point.y;

        float stepAmount = collisionContactY - previousContactY;
        previousContactY = collisionContactY;

        if (stepAmount > 0.005 && stepAmount < .02f) {
            transform.position = Vec3.SetY(transform.position, transform.position.y + stepAmount);
            return true;
        }
        return false;
    }

    private bool arrayContains(string[] strArr, string elem) {
        foreach (string s in strArr) {
            if (s == elem)
                return true;
        }
        return false;
    }

    private void moveBody(float displacement) {
        switch (direction) {
            // body removed from each of clauses
            case 0:
                transform.Translate(-displacement, 0, 0, Space.World);
                break;
            case 1:
                transform.Translate(0, 0, displacement, Space.World);
                break;
            case 2:
                transform.Translate(displacement, 0, 0, Space.World);
                break;
            case 3:
                transform.Translate(0, 0, -displacement, Space.World);
                break;
        }
    }

    private bool collidedInFront(Collision col) {
        foreach (ContactPoint pt in col.contacts) {
            if (contactPointIsFront(pt))
                return true;
        }
        return false;
    }

    private bool contactPointIsFront(ContactPoint pt) { 
        float pointX = pt.point.x;
        float pointZ = pt.point.z;
        float curX = transform.position.x;
        float curZ = transform.position.z;
        float colliderGapSize = transform.localScale.z * gameObject.GetComponent<BoxCollider>().size.z / 2 + .01f;

        switch (direction) {
            case 0:
                return Mathf.Abs(pointZ - curZ) < colliderGapSize && pointX < curX;
            case 1:
                return Mathf.Abs(pointX - curX) < colliderGapSize && pointZ > curZ;
            case 2:
                return Mathf.Abs(pointZ - curZ) < colliderGapSize && pointX > curX;
            default:
                return Mathf.Abs(pointX - curX) < colliderGapSize && pointZ < curZ;
        }
    }

    private void incrementDirection() {
        direction++;
        if (direction > 3)
            direction = 0;
    }

    private void decrementDirection() {
        direction--;
        if (direction < 0)
            direction = 3;
    }

    private Vector3[] getPivotPoints() {
        Vector3 point0 = transform.position;
        Vector3 pointA = transform.position;
        Vector3 pointB = transform.position;

        switch (direction) {
            case 0:
                pointA.x += .6f;
                pointB.x -= .6f;
                break;
            case 1:
                pointA.z -= .6f;
                pointB.z += .6f;
                break;
            case 2:
                pointA.x -= .6f;
                pointB.x += .6f;
                break;
            case 3:
                pointA.z += .6f;
                pointB.z -= .6f;
                break;
        }

        point0.y += .1f;
        pointA.y += .1f;
        pointB.y += .1f;

        Vector3[] points = { point0, pointA, pointB };
        return points;
    }

    private Vector3 getForwardVec() {
        Vector3 forwardVec = new Vector3(0, 0, 0);
        switch (direction) {
            case 0:
                forwardVec.x = -1.0f;
                break;
            case 1:
                forwardVec.z = 1.0f;
                break;
            case 2:
                forwardVec.x = 1.0f;
                break;
            case 3:
                forwardVec.z = -1.0f;
                break;
        }
        return forwardVec;
    }

    private int roundRotation(float eulerAngle) {
        int[] angles = { -360, -270, -180, -90, 0, 90, 180, 270, 360 };
        int minDiff = 360;
        int id = 8;
        int i = 0;

        foreach(int angle in angles) {
            int diff = Mathf.Abs((int)eulerAngle - angle);
            if (diff < minDiff) {
                minDiff = diff;
                id = i;
            }
            i++;
        }

        return angles[id];
    }
}
