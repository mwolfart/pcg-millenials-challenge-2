﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TileRepeater : MonoBehaviour {
    public GameObject Prefab;
    public int repeatAmountX;
    public int repeatAmountZ;
    public int distance = 1;

    // Use this for initialization
    void Start () {
        float startingX = transform.position.x;
        float startingZ = transform.position.z;

        for (float x = startingX; x < startingX + repeatAmountX * distance; x += distance) {
            for (float z = startingZ; z < startingZ + repeatAmountZ * distance; z += distance) {
                Instantiate(Prefab, new Vector3(x, transform.position.y, z), Quaternion.identity);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
