﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotation : MonoBehaviour
{
    public int speed = 10;

    void FixedUpdate()
    {
        gameObject.transform.Rotate(0, speed, 0, Space.World);
    }
}
