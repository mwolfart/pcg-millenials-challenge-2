﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBobbing : MonoBehaviour
{
    public float range = 0.2f;
    public int stDelay = 50;
    private float delta = 0;
    private float acc;
    private int mod = 1;
    private int delay;

    private void Start() {
        acc = range;
        delay = stDelay;
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (delay > 0) {
            delay--;
            return;
        }
        delay = stDelay;

        if (range == 0)
            return;

        if (acc * mod <= 0) {
            mod = -1 * mod;
            acc = mod * 2 * range;
        }

        delta = acc;
        acc = acc - 1 * mod;
        gameObject.transform.Translate(0, delta, 0);
    }
}
