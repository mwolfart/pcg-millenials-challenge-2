﻿using UnityEngine;

public class ThirdPersonCamera : MonoBehaviour {
    public Transform Target;
    public Transform CameraObject;
    public float distance = 10.0f;
    public float sensitivityX = 4.0f;
    public float sensitivityY = 2.0f;

    private Camera adaptedCam;

    private float currentX = 0.0f;
    private float currentY = 40.0f;

    void Start() {
        CameraObject = transform;
        adaptedCam = Camera.main;
    }

    void Update() {
        currentX += Input.GetAxis("Mouse X") * sensitivityX;
        currentY -= Input.GetAxis("Mouse Y") * sensitivityY;
        currentY = Mathf.Clamp(currentY, 20f, 60f);
    }

    void LateUpdate() {
        if (Target == null)
            return;

        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        CameraObject.position = Target.position + rotation * dir;
        CameraObject.LookAt(Target.position);
    }
}
