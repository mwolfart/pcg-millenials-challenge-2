﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonBehavior : MonoBehaviour
{
    public GameObject ConnectedObj;
    public GameObject ButtonObj;

    private bool pressed = false;
    private LevelBehavior levelBehavior;

    void Start() {
        GameObject HUD = GameObject.FindGameObjectWithTag("HUD");
        levelBehavior = HUD.GetComponent<LevelBehavior>();
    }

    void Update() {
        if (pressed && ButtonObj && ButtonObj.gameObject.transform.localPosition.y > 0) {
            ButtonObj.gameObject.transform.localPosition = Vec3.SetY(ButtonObj.gameObject.transform.localPosition, ButtonObj.gameObject.transform.localPosition.y - .05f);
        } else if (!pressed && ButtonObj && ButtonObj.gameObject.transform.localPosition.y < 0.4f) {
            ButtonObj.gameObject.transform.localPosition = Vec3.SetY(ButtonObj.gameObject.transform.localPosition, ButtonObj.gameObject.transform.localPosition.y + .05f);
        }
    }

    public void PressButton() {
        pressed = true;

        switch(gameObject.name.Split(null)[0]) {
            case "Trap":
                if (ConnectedObj) {
                    ConnectedObj.GetComponent<TrapBehavior>().Active = false;
                }
                break;
            case "Toggle":
                levelBehavior.toggleFlag = !levelBehavior.toggleFlag;
                break;
            default:
                break;
        }
    }

    public void ReleaseButton() {
        pressed = false;

        switch (gameObject.name.Split(null)[0]) {
            case "Trap":
                if (ConnectedObj) {
                    ConnectedObj.GetComponent<TrapBehavior>().Active = true;
                }
                break;
            case "Toggle":
                break;
            default:
                break;
        }
    }
}
