﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehavior : MonoBehaviour {
    private GameObject waterParticles;
    private GameObject fireParticles;
    private GameObject bombParticles;
    private int i = 0;

    void Start() {
        // Particles
        waterParticles = Resources.Load<GameObject>("Millenials/Particles/Water Particles");
        fireParticles = Resources.Load<GameObject>("Millenials/Particles/Fire Particles");
        bombParticles = Resources.Load<GameObject>("Millenials/Particles/Bomb Particles");
        gameObject.GetComponent<Rigidbody>().sleepThreshold = 0;
    }

    void OnCollisionEnter(Collision collision) {
        switch (collision.gameObject.tag) {
            case "Bomb":
                Vector3 particlePosition = Vec3.SetY(gameObject.transform.position, 0.5f);
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/jezzdead");
                spawnParticles(particlePosition, "Bomb");
                Destroy(collision.gameObject);
                Destroy(gameObject);
                break;
                /*
            case "Lift":
                collision.gameObject.GetComponent<LiftBehavior>().ToggleRaised();
                break;
                */
            default:
                break;
        }
    }

    void OnCollisionStay(Collision collision) {
        switch (collision.gameObject.tag) {
            case "Trap":
                if (collision.gameObject.GetComponent<TrapBehavior>().Active) {
                    setEnemyFrozen(true);
                } else {
                    Vector3 enemyPosition = collision.transform.position;
                    enemyPosition.y++;
                    transform.position = enemyPosition;
                    setEnemyFrozen(false);
                }
                break;
            default:
                break;
        }
    }

    void OnTriggerEnter(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Fire":
                Vector3 particlePosition = Vec3.SetY(gameObject.transform.position, 0.5f);
                spawnParticles(particlePosition, "Fire");
                Destroy(gameObject);
                break;
            case "Button":
                trigger.gameObject.GetComponent<ButtonBehavior>().PressButton();
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/pop2");
                break;
            default:
                break;
        }
    }

    void OnTriggerStay(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Water":
                testWaterCollision(trigger);
                break;
            default:
                break;
        }
    }

    void OnTriggerExit(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Button":
                trigger.gameObject.GetComponent<ButtonBehavior>().ReleaseButton();
                break;
            default:
                break;
        }
    }

    private void testWaterCollision(Collider waterTrigger) {
        bool distanceTooClose = transform.position.y - .25f < waterTrigger.transform.position.y;
        if (!distanceTooClose)
            return;

        // TODO maybe fix X and Z

        Vector3 particlePosition = Vec3.SetY(gameObject.transform.position, 0.5f);
        spawnParticles(particlePosition, "Water");
        gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/water2");
        Destroy(gameObject);
    }

    private void spawnParticles(Vector3 particlePosition, string particleType = "") {
        GameObject particlePrefab = getDeathParticlePrefab(particleType);
        GameObject particles = Instantiate(particlePrefab, particlePosition, Quaternion.identity);
        particles.GetComponent<ParticleSystem>().Play();
    }

    private GameObject getDeathParticlePrefab(string mode = "") {
        switch (mode) {
            case "Water":
                return waterParticles;
            case "Fire":
                return fireParticles;
            default:
                return bombParticles;
        }
    }

    private void setEnemyFrozen(bool freeze) {
        EnemyRotate enemyRotate = gameObject.GetComponent<EnemyRotate>();
        EnemyMovePlane enemyMoveP = gameObject.GetComponent<EnemyMovePlane>();
        //EnemyWallMove enemyMoveW = gameObject.GetComponentInParent<EnemyWallMove>();
        EnemyWallMove enemyMoveW = gameObject.GetComponent<EnemyWallMove>();
        if (enemyRotate) enemyRotate.freezeMovement = freeze;
        if (enemyMoveP) enemyMoveP.freezeMovement = freeze;
        if (enemyMoveW) enemyMoveW.freezeMovement = freeze;
    }
}
