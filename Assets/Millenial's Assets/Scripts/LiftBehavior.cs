﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftBehavior : MonoBehaviour
{
    private bool isRaised = false;
    private Vector3 raisedPos;
    private Vector3 loweredPos;

    void Start() {
        loweredPos = transform.position;
        loweredPos.y = transform.position.y;
        raisedPos = transform.position;
        raisedPos.y = transform.position.y + 1;
    }

    void Update() {
        if (isRaised) {
            transform.position = Vector3.MoveTowards(transform.position, raisedPos, 0.05f);
        } else {
            transform.position = Vector3.MoveTowards(transform.position, loweredPos, 0.05f);
        }
    }

    public void ToggleRaised() {
        isRaised = !isRaised;
    }
}
