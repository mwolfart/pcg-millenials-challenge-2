﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleBehavior : MonoBehaviour {
    public bool isDown = false;

    public GameObject blockBase;
    public GameObject blockPlatform;
    public GameObject blockTop;

    private LevelBehavior levelBehavior;
    
    private Transform baseTransform { get { return blockBase.transform; } }
    private Transform platTransform { get { return blockPlatform.transform; } }
    private Transform topTransform { get { return blockTop.transform; } }

    void Start() {
        GameObject HUD = GameObject.FindGameObjectWithTag("HUD");
        levelBehavior = HUD.GetComponent<LevelBehavior>();
    }

    void Update() {
        bool toggleFlag = levelBehavior.toggleFlag;
        if ((toggleFlag && isDown) || (!toggleFlag && !isDown))
            raiseBlock();
        else lowerBlock();
    }

    private void raiseBlock() {
        gameObject.GetComponent<BoxCollider>().enabled = true;
        
        if (shouldRaiseBase()) {
            baseTransform.localScale = moveVecPropTowards(baseTransform.localScale, 'z', 0.1f, 0.1f);
            baseTransform.localPosition = moveVecPropTowards(baseTransform.localPosition, 'y', 0.1f, 0.1f);
        } else if (shouldRaisePlatform()) {
            platTransform.localScale = moveVecPropTowards(platTransform.localScale, 'z', 0.85f, 0.1f);
            platTransform.localPosition = moveVecPropTowards(platTransform.localPosition, 'y', 1.05f, 0.1f);
            topTransform.localPosition = moveVecPropTowards(topTransform.localPosition, 'y', 1.905f, 0.2f);
        }
    }

    private void lowerBlock() {
        gameObject.GetComponent<BoxCollider>().enabled = false;

        if (shouldLowerBase()) {
            baseTransform.localScale = moveVecPropTowards(baseTransform.localScale, 'z', 0.025f, 0.1f);
            baseTransform.localPosition = moveVecPropTowards(baseTransform.localPosition, 'y', 0.025f, 0.1f);
        } else if (shouldLowerPlatform()) {
            platTransform.localScale = moveVecPropTowards(platTransform.localScale, 'z', 0.01f, 0.1f);
            platTransform.localPosition = moveVecPropTowards(platTransform.localPosition, 'y', 0.055f, 0.1f);
            topTransform.localPosition = moveVecPropTowards(topTransform.localPosition, 'y', 0.065f, 0.2f);
        }
    }

    private bool shouldRaiseBase() {
        return ((baseTransform.localScale.z < 0.1f
            || baseTransform.localPosition.y < 0.1f)
            && (platTransform.localPosition.y > 0.2f));
    }

    private bool shouldLowerBase() {
        return ((baseTransform.localScale.z > 0.025f
            || baseTransform.localPosition.y > 0.025f)
            && (platTransform.localPosition.y < 0.2f));
    }

    private bool shouldRaisePlatform() {
        return (platTransform.localScale.z < 0.85f
            || platTransform.localPosition.y < 1.05f
            || topTransform.localPosition.y < 1.905f);
    }

    private bool shouldLowerPlatform() {
        return (platTransform.localScale.z > 0.01f
            || platTransform.localPosition.y > 0.055f
            || topTransform.localPosition.y > 0.065f);
    }


    private Vector3 moveVecPropTowards(Vector3 sourceVec, char coord, float targetValue, float speed) {
        Vector3 targetVec = sourceVec;
        targetVec[coord - 'x'] = targetValue;
        return Vector3.MoveTowards(sourceVec, targetVec, speed);
    }
}
