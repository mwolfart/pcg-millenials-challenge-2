using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveJump : MonoBehaviour {
    public float startingSpeed = 1f;
    public float gravityAcceleration = .01f;
    private float speed;

    void Start() {
        speed = startingSpeed;
    }

    void Update() {
        transform.Translate(0, 2 * Time.deltaTime * speed, 0, Space.World);
        speed -= gravityAcceleration;
    }

    void OnCollisionEnter(Collision col) {
        if (col.gameObject.tag != "Player") {
            speed = startingSpeed;
        }
    }
}
