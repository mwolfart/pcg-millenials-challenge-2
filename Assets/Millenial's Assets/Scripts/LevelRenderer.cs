﻿using UnityEngine;
using System.IO;
using System;
using System.Collections.Generic;

struct Tile {
    char type;
    int subType;
    char orientation;

    public Tile(char type, int subType, char orientation) {
        this.type = type;
        this.subType = subType;
        this.orientation = orientation;
    }
}

struct InsertOperation {
    public char typeOp;
    public int objType;
    public int x, y;
    public char orientation;

    public InsertOperation(int x, int y, char typeOp, int objType, char orientation = 'N') {
        this.typeOp = typeOp;
        this.x = x;
        this.y = y;
        this.objType = objType;
        this.orientation = orientation;
    }
}

class TileLayer {
    private Tile[,] tiles;

    public TileLayer(int layerSize) {
        tiles = new Tile[layerSize, layerSize];
    }

    public void set(int i, int j, Tile t) {
        tiles[i, j] = t;
    }

    public Tile get(int i, int j) {
        return tiles[i,j];
    }
}

public class LevelRenderer : MonoBehaviour {
    public string filePath;
    private List<TileLayer> objectLayers = new List<TileLayer>();

    // Use this for initialization
    void Start() {
        ParseLevelFile(filePath);
    }

    // Update is called once per frame
    void Update() {

    }

    void ParseLevelFile(string filePath) {
        string line;
        StreamReader fStream = new StreamReader(filePath);

        string lSize = fStream.ReadLine();
        string lTheme = fStream.ReadLine();
        string lMusic = fStream.ReadLine();

        int levelSize = parseInt(removeComments(lSize));
        int levelTheme = parseInt(removeComments(lTheme));
        int levelMusic = parseInt(removeComments(lMusic));

        while ((line = removeComments(fStream.ReadLine())) != null) {
            int numLinesForLayer = parseInt(line);
            TileLayer layer = new TileLayer(levelSize);
            this.objectLayers.Add(layer);

            while (numLinesForLayer > 0) {
                line = removeComments(fStream.ReadLine());

                ParseLineOperationsAndExecute(line);

                numLinesForLayer--;
            }
        }
    }

    // TODO This function can be drastically improved. We should use stacks and split the strings into an array. Not a priority though 
    void ParseLineOperationsAndExecute(string line) {
        List<InsertOperation> operationList = new List<InsertOperation>();
        int repeatY = -1, repeatX = -1, repeatStartX = 0, repeatStartY = 0, x = 0, y = 0, type;
        InsertOperation op;

        for (int i = 0; i < line.Length; i++) {
            bool insideCellLoop = (repeatX != -1);
            bool insideLineLoop = (repeatY != -1);

            if (!insideCellLoop) {
                x = parseInt(line[i]);
                i += 2;
            }

            if (!insideCellLoop && !insideLineLoop) {
                y = parseInt(line[i]);
                i += 2;
            }

            char ch = line[i];
            switch (ch) {
                case 'E':
                    type = parseInt(new string(line[i+2], line[i+3]));
                    char orientation = line[i+5];
                    op = new InsertOperation(x, y, 'E', type, orientation);
                    operationList.Add(op);
                    i += 6;
                    break;
                case 'B':
                    type = parseInt(new string(line[i+2], line[i+3]));
                    op = new InsertOperation(x, y, 'B', type);
                    operationList.Add(op);
                    i += 5;
                    break;
                case 'I':
                    type = parseInt(new string(line[i+2], line[i+3]));
                    op = new InsertOperation(x, y, 'I', type);
                    operationList.Add(op);
                    i += 5;
                    break;
                case '[':
                    if (!insideLineLoop) {
                        // start cell loop (repeat these cells N times horizontally)
                        repeatStartY = operationList.Count;
                        repeatY = y;
                    } else {
                        // start line loop (repeat these cells N times vertically)
                        repeatStartX = operationList.Count;
                        repeatX = x;
                    }
                    break;
                case ']':
                    int repeatEnd = operationList.Count;
                    int repeatAmount = parseInt(line[i+1]);

                    if (insideCellLoop) {
                        // repeat cells
                        for (int it = 1; it < repeatAmount; it++) {
                            for (int opId = repeatStartX; opId < repeatEnd; opId++) {
                                InsertOperation currentOp = operationList[opId];
                                InsertOperation duplicateOp = new InsertOperation(repeatX + it, currentOp.y, currentOp.typeOp, currentOp.objType, currentOp.orientation);
                                operationList.Add(duplicateOp);
                            }
                        }
                        repeatX = -1;
                    } else if (insideLineLoop) {
                        // repeat lines (repeated cells will already be in place)
                        for (int it = 1; it < repeatAmount; it++) {
                            for (int opId = repeatStartY; opId < repeatEnd; opId++) {
                                InsertOperation currentOp = operationList[opId];
                                InsertOperation duplicateOp = new InsertOperation(currentOp.x, repeatY + it, currentOp.typeOp, currentOp.objType, currentOp.orientation);
                                operationList.Add(duplicateOp);
                            }
                        }
                        repeatY = -1;
                    }
                    i += 2;
                    break;
                case 'P':
                    op = new InsertOperation(x, y, 'P', 0);
                    operationList.Add(op);
                    i += 1;
                    break;
            }
        }

        // after saving the instructions, execute them
        foreach (InsertOperation oper in operationList) {
            Tile tile = new Tile(oper.typeOp, oper.objType, oper.orientation);
            this.objectLayers[-1].set(oper.x, oper.y, tile);
        }
    }

    string removeComments(string line) {
        return (line.IndexOf("#") != -1) ? line.Substring(0, line.IndexOf("#")) : line;
    }

    int parseInt(string s) {
        return Int32.Parse(s);
    }

    int parseInt(char c) {
        return c - '0';
    }
}
