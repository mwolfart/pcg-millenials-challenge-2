﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirtBlockInteractions : MonoBehaviour
{
    private GameObject dirtPrefab;
    private GameObject bombParticles;
    private bool blockIsUsable = true;

    public bool debug = false;

    void Start() {
        dirtPrefab = Resources.Load<GameObject>("Millenials/Interactables/Dirt");
        bombParticles = Resources.Load<GameObject>("Millenials/Particles/Bomb Particles");
        //gameObject.GetComponent<Rigidbody>().sleepThreshold = 0;
    }

    void FixedUpdate() {
        adjustXZMovement();
    }

    void OnCollisionStay(Collision collision) {
        switch (collision.gameObject.tag) {
            case "Suction":
                break;
            case "Ice":
                break;
            case "Button":
                break;
            case "Bomb":
                spawnBombParticles(Vec3.SetY(collision.gameObject.transform.position, .2f));
                Destroy(collision.gameObject);
                Destroy(gameObject);
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/jezzdead");
                break;
            case "Trap":
                testTrapCollision(collision);
                break;
            default:
                break;
        }
    }

    void OnTriggerStay(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Water":
                if (Utils.testDistanceBetweenCenters(transform.position, trigger.gameObject.transform.position, 0.51f))
                    testWaterCollision(trigger);
                break;
            default:
                break;
        }
    }

    void OnTriggerEnter(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Button":
                trigger.gameObject.GetComponent<ButtonBehavior>().PressButton();
                gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/pop2");
                break;
            default:
                break;
        }
    }

    void OnTriggerExit(Collider trigger) {
        switch (trigger.gameObject.tag) {
            case "Button":
                trigger.gameObject.GetComponent<ButtonBehavior>().ReleaseButton();
                break;
            default:
                break;
        }
    }

    private void testWaterCollision(Collider waterTrigger) {        
        bool distanceTooClose = transform.position.y < waterTrigger.transform.position.y;
        if (!distanceTooClose || !blockIsUsable)
            return;

        blockIsUsable = false;
        Vector3 dirtPosition = waterTrigger.transform.position;
        dirtPosition.y = 0.2f;

        Destroy(waterTrigger.gameObject);
        Instantiate(dirtPrefab, dirtPosition, Quaternion.identity);

        gameObject.GetComponent<SoundPlayer>().PlaySound("Millenials/Audio/water2");
        Destroy(gameObject);
    }

    private void testTrapCollision(Collision trapCollider) {
        bool distanceTooClose = transform.position.y < trapCollider.transform.position.y + 0.5f;
        if (!distanceTooClose)
            return;

        if (!trapCollider.gameObject.GetComponent<TrapBehavior>().Active) {
            transform.position = Vec3.SetY(transform.position, 0.5f);
        } else {
            transform.position = Vec3.SetX(transform.position, trapCollider.transform.position.x);
            transform.position = Vec3.SetZ(transform.position, trapCollider.transform.position.z);
        }
    }

    private void adjustXZMovement() {
        Vector3 velocity = gameObject.GetComponent<Rigidbody>().velocity;
        float velY = Mathf.Round(velocity.y * 100f) / 100f;
        if (velY < 0) {
            freezeRigidBodyXZ();
        } else {
            unfreezeRigidBodyXZ();
            clearLowestBetweenXZ(velocity);
        }
    }

    private void freezeRigidBodyXZ() {
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX
                | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ
                | RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
    }

    private void unfreezeRigidBodyXZ() {
        gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX
                | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
    }

    private void clearLowestBetweenXZ(Vector3 velocity) {
        if (Mathf.Abs(velocity.x) >= Mathf.Abs(velocity.z))
            gameObject.GetComponent<Rigidbody>().velocity = Vec3.ClearZ(velocity);
        else gameObject.GetComponent<Rigidbody>().velocity = Vec3.ClearX(velocity);
    }

    private void spawnBombParticles(Vector3 particlePosition) {
        GameObject particles = Instantiate(bombParticles, particlePosition, Quaternion.identity);
        particles.GetComponent<ParticleSystem>().Play();
    }
}
