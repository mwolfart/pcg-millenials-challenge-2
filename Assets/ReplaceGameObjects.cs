﻿using UnityEngine;
using UnityEditor;
using System.Collections;
// CopyComponents - by Michael L. Croswell for Colorado Game Coders, LLC
// March 2010

public class ReplaceGameObjects : ScriptableWizard {
    public bool copyValues = true;
    public GameObject ReplaceWith;
    public GameObject[] ObjsToReplace;

    [MenuItem("Custom/Replace GameObjects")]


    static void CreateWizard() {
        ScriptableWizard.DisplayWizard("Replace GameObjects", typeof(ReplaceGameObjects), "Replace");
    }

    void OnWizardCreate() {
        foreach (GameObject rep in ObjsToReplace) {
            Transform[] Replaces;
            Replaces = rep.GetComponentsInChildren<Transform>();

            Transform t = Replaces[0];
            GameObject newObject;
            newObject = (GameObject)EditorUtility.InstantiatePrefab(ReplaceWith);
            newObject.transform.position = t.position;
            newObject.transform.rotation = t.rotation;
            newObject.transform.localScale = t.localScale;

            DestroyImmediate(t.gameObject);
        }


    }
}